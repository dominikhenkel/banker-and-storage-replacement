import sys
import math
import copy

def get_lru_index(table, target_column):
    index = 0
    value = sys.maxsize
    for i in range(len(table)):
        if table[i][target_column] < value:
            index = i
            value = table[i][target_column]
    return index

def run_lru(table, pagesToInsert, startLoadingTime):
    for i in range(len(pagesToInsert)):
        existing_element = list(filter(lambda x: x[1] == pagesToInsert[i], table))
        if len(existing_element) > 0:
            existing_element = existing_element[0]
            existing_element[3] = startLoadingTime
            existing_element[4] = 1
            existing_element[5] = 1
            print(existing_element)
        else: 
            lru_index = get_lru_index(table, 3)
            table[lru_index] = [table[lru_index][0], pagesToInsert[i], startLoadingTime, startLoadingTime, 1, 1]
            print(table[lru_index])

        startLoadingTime += 10
    print("\nResultierende Tabelle:\n",table)

def roundup(x):
    return int(math.ceil(x / 10.0)) * 10


def run_fifosc(table, pagesToInsert, startLoadingTime):
    inserted = 0
    while(inserted < len(pagesToInsert)):
        existing_element = list(filter(lambda x: x[1] == pagesToInsert[inserted], table))
        if len(existing_element) > 0:
            existing_element = existing_element[0]
            existing_element[3] = startLoadingTime
            existing_element[4] = 1
            existing_element[5] = 1
            print(existing_element)
            inserted = inserted + 1
            startLoadingTime = roundup(startLoadingTime + 1)
        else:
            lru_index = get_lru_index(table, 2)
            if(table[lru_index][4] == 1):
                table[lru_index][2] = startLoadingTime
                table[lru_index][4] = 0
                startLoadingTime = startLoadingTime + 1
            else:
                table[lru_index] = [table[lru_index][0], pagesToInsert[inserted], startLoadingTime, startLoadingTime, 1, 1]
                inserted = inserted + 1
                startLoadingTime = roundup(startLoadingTime)
            print(table[lru_index])
    print("\nResultierende Tabelle:\n",table)
            

def getStartLoadingTime():
    print("Startzeit eingeben")
    return int(input())

def getPagesToInsert():
    print("Seiten eingeben, die in entsprechender Reihenfolge hinzugefügt werden sollen (mit Leerzeichen trennen)")
    user_input = input()
    pages = user_input.split(' ')
    return [int(i) for i in pages]

def fill_table():
    table = []
    print("Zeile eingeben oder mit 'c' fortfahren wählen:\nRahmen Seite Ladezeit LetzterZugriff r-Bit d-Bit (mit Leerzeichen trennen)")
    user_input = input()
    while user_input != "c":
        row = user_input.split(' ')
        if len(row) == 6:
            table.append([int(i) for i in row])
        else:
            print("ungültige Eingabe.")
        user_input = input()
    return table


def main():
    table = fill_table()
    pagesToInsert = getPagesToInsert()
    startLoadingTime = getStartLoadingTime()
    print("\nLRU")
    run_lru(copy.deepcopy(table), pagesToInsert, startLoadingTime)

    print("\nFIFO Second Chance")
    run_fifosc(copy.deepcopy(table), pagesToInsert, startLoadingTime)

if __name__ == "__main__":
    main()
