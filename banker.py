import operator

def getExistingResources():
    print("Existierende Ressourcen (E) eingeben (mit Leerzeichen trennen)")
    vector = input().split(' ')
    return [int(i) for i in vector]

def calculateFreeRessources(e, c):
    a = []
    for i in range(len(e)):
        sumC = 0
        for j in range(len(c)):
            sumC = sumC + c[j][i]
        a.append(e[i] - sumC)
    return a
        
def getInputMatrix(requiredRowNum, inputText):
    table = []
    print(inputText)
    user_input = input()
    while user_input != "c":
        row = user_input.split(' ')

        if(len(row) != requiredRowNum):
            raise ArithmeticError("column length doesn't fit to previous entered vectors")
        table.append([int(i) for i in row])
        user_input = input()
    return table

def doesFit(a, row):
    for i in range(len(a)):
        if row[i] > a[i] :
            return False
    return True

def getNextRow(currentRow, lengthOfR, foundRowThisIteration):
    if (currentRow + 1) < lengthOfR:
        return currentRow + 1
    elif foundRowThisIteration:
        return 0
    else:
        return -1

def run_banker(e, c, a, r):
    rowsLeft = [i for i in range(len(r))]
    currentRow = 0
    foundRowThisIteration = False
    while len(rowsLeft) > 0:
        if currentRow == 0:
            foundRowThisIteration = False
        if currentRow in rowsLeft:
            if doesFit(a, r[currentRow]):
                rowsLeft.remove(currentRow)
                a = list(map(operator.add, a, c[currentRow]))
                foundRowThisIteration = True
                print(currentRow, "terminiert. A:", a, "In R existieren noch die Zeilen", rowsLeft)   
            currentRow = getNextRow(currentRow, len(r), foundRowThisIteration)
        else:
            currentRow = getNextRow(currentRow, len(r), foundRowThisIteration)
        if currentRow == -1:
            print("kein Prozess mehr terminierbar.\n-> Es handelt sich um einen unsicheren Zustand.")
            return False
    print("-> Es handelt sich um einen sicheren Zustand.")

def main():
    e = getExistingResources()
    c = getInputMatrix(len(e), "Aktuell belegte Ressourcen (C) eingeben (mit Leerzeichen trennen) oder mit 'c' fortfahren wählen")
    a = calculateFreeRessources(e, c)
    print("Errechnete, freie Ressourcen:\n", a)
    r = getInputMatrix(len(e), "Noch benötigte Ressourcen (R) eingeben (mit Leerzeichen trennen) oder mit 'c' fortfahren wählen")
    if(len(c[0]) != len(r[0])):
            raise ArithmeticError("column length doesn't fit to previous entered vectors")
    run_banker(e, c, a, r)

if __name__ == "__main__":
    main()